stage { 'repos' : before => Stage['main'] }

class yumrepos {
  package { 'epel-release':
     provider => 'rpm',
     ensure => installed,
     source => 'http://mirror.pnl.gov/epel/6/x86_64/epel-release-6-8.noarch.rpm'
  }

  package { 'ius-release':
     provider => 'rpm',
     ensure => installed,
     source => 'http://dl.iuscommunity.org/pub/ius/stable/CentOS/6/x86_64/ius-release-1.0-11.ius.centos6.noarch.rpm',
     require => Package['epel-release']
  }

  yumrepo { 'shawn':
     baseurl => "http://www.betterwithmonkeys.com/packages/centos/\$releasever/\$basearch",
     descr => 'FPM Created RPMs by Shawn',
     enabled => 1,
     gpgcheck => 0,
  }

}

node default {
  class { 'yumrepos': stage => 'repos' }
} 

package { 'python27':
     ensure => installed
}

package { 'python27-distribute':
    ensure => installed,
    require => Package['python27']
}

# easypip (fork of python-pip that provides hybrid easy_install/pip support
# is dumb and require easy_install to be at /usr/local/bin/easy_install
# modules/ should contain a got cone of: https://github.com/wayfair/puppet-pip
file { '/usr/local/bin/easy_install':
   ensure => 'link',
   target => '/usr/bin/easy_install-2.7',
   require => Package['python27-distribute']
}

# Boostrap pip by installing it with easy_install via easypip
package { 'pip':
   ensure => present,
   source => 'https://pypi.python.org/packages/source/p/pip/pip-1.4.1.tar.gz#md5=6afbb46aeb48abac658d4df742bff714',
   provider => 'easypip',
   require => File['/usr/local/bin/easy_install']
}

package { "virtualenv":
          ensure  => present,
          provider => "pip",
	  require => Package['pip']
}

$scipyfamily = [ "numpy", "ipython", "sympy", "pandas" ]
# matplotlib
# "You are installing an externally hosted file. Future versions of pip will default to disallowing externally hosted files."
# See "Externally hosted files" on http://www.pip-installer.org/en/latest/logic.html

package { 
  $scipyfamily: ensure => "installed",
  provider => "pip",
  require => Package['pip']
}

# Scipy itself has dependencies that need to be met
$scipydeps = [ "blas", "blas-devel", "lapack", "lapack-devel" ]

package {
  $scipydeps: ensure => "installed",
}

package { "scipy":
	ensure => "installed",
	provider => "pip",
	require => [ Package['pip'], Package['numpy'], Package['blas-devel'], Package['lapack-devel'] ]
}

$pipconf = "[install]
allow-insecure=matplotlib
"

file { '/etc/pip.conf':
  content => $pipconf
}

# Matplotlib has a number of build deps, especially if building all optional features
# See http://matplotlib.org/users/installing.html
# OS: libpng-devel, freetype-devel
# Python: pyparsing
# Bundled with matplotlib: pycxx, libagg
# Optional support: (yum) tkinter27, (yum) tk-devel, 

$matplotlibdeps = [ 'libpng-devel', 'freetype-devel', 'tkinter27', 'tk-devel' ]

package {
  $matplotlibdeps: ensure => "installed",
}

$matplotlibpydeps = [ 'python-dateutil', 'pyparsing' ]
package {
  $matplotlibpydeps: ensure => "installed",
  provider => 'pip',
  require => Package['pip']
}

# Can't pip install pygtk, so no gtk support: (yum) gtk2-devel, (pip) pygtk
# Can't pip install pyqt4, so no Qt support. (yum) qt-devel, (pip) PyQt4

package { "matplotlib":
	ensure => "installed",
	provider => "pip",
	require => [ Package['pip'], File['/etc/pip.conf'], Package['numpy'], Package['python-dateutil'], 
			Package['tornado'], Package['libpng-devel'], Package['freetype-devel'],
			Package['tkinter27'], Package['tk-devel'], Package['pyparsing'] ]
}

# We want to use the notebook feature of ipython which has additional dependencies
$ipynbdeps = [ 'tornado', 'jinja2', 'pyzmq' ]

package { 
  $ipynbdeps: ensure => "installed",
  provider => "pip",
  require => Package['pip']
}

package { 'rubygem-fpm':
  ensure => "installed",
}


# BeautifulSoup, python-ldap (yumdep: openldap-devel)
# psycopg2 (postgres, yumdep: postgresql-devel), mysql-connector-python
# sqlalchemy
# virtualenvwrapper, nose, nosexunit
# pylint, pep8, pyflakes, pep381client
# coverage - currently 3.6 but for some reason nosexunit wants exactly v == 2.85
# http://nedbatchelder.com/code/modules/coverage-2.85.tar.gz
# requests, feedparser, netaddr, django, flask, web.py
# ius includes python27-lxml and python27-mod_wsgi
# Not in PyPI or not installable via easy_install/pip: MySQLdb, pygtk, PyQT4 
# Pydeps: nosexunit -> pygments kid
#	  jinja2 -> markupsafe
#	  flask -> werkzeug itsdangerous
# 	  pandas -> pytz
#	  virtualenvwrapper -> stevedore virtualenv-clone
#	  pylint -> astroid logilab-common
#	  dateutil -> six

# git, hg, svn

# Use fpm to build the python packages and the rubygems?
# fpm -s python --python-bin /usr/bin/python2.7 --python-package-name-prefix=python27 --python-easyinstall /usr/bin/easy_install-2.7 -t rpm --verbose "packagename"
# ruby deps for fpm: childprocess, clamp, ftw, arr-pm, backports, cabin, json, ffi, http_parsea.rbr, addressable

# fpm equivalent of something like homebrew
# fpm-cookery https://github.com/bernd/fpm-cookery

# Serving Files with Puppet Standalone in Vagrant From the puppet:// URIs
# http://java.dzone.com/articles/serving-files-puppet

# ./configure && make && make install ??
# puppet-module-build
# https://github.com/jfqd/puppet-module-build
# 
# Example of OpenNTPD with Exec
# http://projects.puppetlabs.com/projects/1/wiki/Open_Ntpd_Patterns
#
# fpm -> rpm or puppet module
# Use Case - Package something that uses 'make install'
# https://github.com/jordansissel/fpm/wiki/PackageMakeInstall
